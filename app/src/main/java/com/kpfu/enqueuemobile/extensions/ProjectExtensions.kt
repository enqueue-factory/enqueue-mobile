package com.kpfu.enqueuemobile.extensions

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStore

fun String?.emptyIfNull(): String {
    return if (this == "null" || this == "Null") {
        ""
    } else this ?: ""
}

val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "settings")
