package com.kpfu.enqueuemobile

import com.github.terrakok.cicerone.androidx.FragmentScreen
import com.kpfu.enqueuemobile.ui.active.ActiveFragment
import com.kpfu.enqueuemobile.ui.home.HomeFragment
import com.kpfu.enqueuemobile.ui.scanner.ScannerFragment

object Screens {

    fun HomeScreen() = FragmentScreen { HomeFragment() }
    fun ScannerScreen() = FragmentScreen { ScannerFragment() }
    fun ActiveScreen() = FragmentScreen { ActiveFragment() }

}
