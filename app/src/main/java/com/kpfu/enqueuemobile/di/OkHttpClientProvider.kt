package com.kpfu.enqueuemobile.di

import android.util.Log
import com.kpfu.enqueuemobile.BuildConfig
import com.kpfu.enqueuemobile.model.data.server.interceptors.CurlLoggingInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit
import javax.inject.Provider


class OkHttpClientProvider : Provider<OkHttpClient> {
    private val connectTimeout = 30L
    private val readTimeout = 30L

    override fun get(): OkHttpClient = with(OkHttpClient.Builder()) {
        connectTimeout(connectTimeout, TimeUnit.SECONDS)
        readTimeout(readTimeout, TimeUnit.SECONDS)

        if (BuildConfig.DEBUG) {
            addInterceptor(HttpLoggingInterceptor(object : HttpLoggingInterceptor.Logger {
                override fun log(message: String) {
                    if (!message.contains("�")) {
                        Log.d(null, message)
                    }
                }
            }).apply {
                level = HttpLoggingInterceptor.Level.BODY
            })
            addNetworkInterceptor(CurlLoggingInterceptor())
        }

        build()
    }
}