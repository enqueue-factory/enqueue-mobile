package com.kpfu.enqueuemobile.di

import com.google.gson.Gson
import com.kpfu.enqueuemobile.BuildConfig
import com.kpfu.enqueuemobile.model.data.server.EnqueueApi
import com.kpfu.enqueuemobile.model.entities.ServerInfo
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Provider

class ApiProvider(private val client: OkHttpClient, val gson: Gson) : Provider<EnqueueApi> {

    override fun get(): EnqueueApi {
        val serverInfo = ServerInfo(
            BuildConfig.SERVER_PROTOCOL,
            BuildConfig.SERVER_HOST,
            BuildConfig.SERVER_PORT.toInt()
        )
        val url = getUrlFromServerInfo(serverInfo)
        return Retrofit.Builder()
            .baseUrl(url)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
            .create(EnqueueApi::class.java)
    }

    private fun getUrlFromServerInfo(info: ServerInfo): String {
        return if (info.port != 0) {
            "${info.protocol}://${info.host}:${info.port}/"
        } else {
            "${info.protocol}://${info.host}/"
        }
    }
}