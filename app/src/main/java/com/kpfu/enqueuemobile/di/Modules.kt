package com.kpfu.enqueuemobile.di

import com.github.terrakok.cicerone.Cicerone
import com.google.gson.GsonBuilder
import com.kpfu.enqueuemobile.model.data.repository.MainRepository
import com.kpfu.enqueuemobile.model.data.server.EnqueueApi
import com.kpfu.enqueuemobile.model.data.storage.DataStorage
import com.kpfu.enqueuemobile.model.data.storage.DataStoragePrefsImplementation
import com.kpfu.enqueuemobile.model.system.ResourceManager
import com.kpfu.enqueuemobile.model.system.ResourceManagerImplementation
import com.kpfu.enqueuemobile.presentation.active.ActiveScreenViewModel
import com.kpfu.enqueuemobile.presentation.home.HomeScreenViewModel
import com.kpfu.enqueuemobile.presentation.scanner.ScannerScreenViewModel
import com.kpfu.enqueuemobile.presentation.splash.SplashScreenViewModel
import okhttp3.OkHttpClient
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.bind
import org.koin.dsl.module

val AppModule = module {

    //region viewModels
    viewModel { SplashScreenViewModel(get(), get(), get()) }
    viewModel { ScannerScreenViewModel(get(), get(), get()) }
    viewModel { HomeScreenViewModel(get()) }
    viewModel { ActiveScreenViewModel(get(), get(), get()) }

    //region repositories
    single { MainRepository(get(), get()) }

    //region storage
    single<DataStorage> { DataStoragePrefsImplementation(get()) }

    //region server
    single { ApiProvider(get(), get()).get() } bind EnqueueApi::class
    single { OkHttpClientProvider().get() } bind OkHttpClient::class
    single { GsonBuilder().create() }

    //other
    val cicerone = Cicerone.create()
    single { cicerone.router }
    single { cicerone.getNavigatorHolder() }

    single { ResourceManagerImplementation(get()) } bind ResourceManager::class
}
