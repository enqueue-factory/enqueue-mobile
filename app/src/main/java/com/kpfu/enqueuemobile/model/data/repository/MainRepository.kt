package com.kpfu.enqueuemobile.model.data.repository

import com.kpfu.enqueuemobile.model.data.server.EnqueueApi
import com.kpfu.enqueuemobile.model.data.server.Result
import com.kpfu.enqueuemobile.model.data.server.ServerError
import com.kpfu.enqueuemobile.model.data.storage.DataStorage
import com.kpfu.enqueuemobile.model.entities.UserQueueInfo
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import okhttp3.ResponseBody

class MainRepository(val dataStorage: DataStorage, val api: EnqueueApi) :
    BaseRepository() {

    var deviceId: String? = null

    val queueInfoFlow: Flow<Result<UserQueueInfo>> = flow {
        while (true) {
            val queueId = getQueueId()
            queueId?.let {
                val result = safeApiCall {
                    api.getUserQueueInfo(
                        deviceId ?: throw IllegalArgumentException("FID не установлен")
                    )
                }
                emit(result)
                when (result) {
                    is Result.Success -> {

                    }
                    is Result.Error -> {
                        if (result.exception is ServerError) {
                            dataStorage.clearCache()
                        }
                    }
                }
            }
            delay(5000)
        }
    }

    suspend fun getUserQueueInfo(): Flow<Result<UserQueueInfo>> = flow {
        while (true) {
            val res = safeApiCall {
                api.getUserQueueInfo(
                    deviceId ?: throw IllegalArgumentException("FID не установлен")
                )
            }
            emit(res)
            if (res is Result.Success) {
                dataStorage.setCurrentQueueId(res.data.queue.name)
                break
            }
            if (res is Result.Error) {
                if (res.exception is ServerError) {
                    break
                }
            }

        }
    }

    fun getQueueIdFlow() = dataStorage.getQueueIdFlow()

    fun getQueueId() = dataStorage.getCurrentQueueId()


    suspend fun enterQueue(queueId: String, username: String): Result<UserQueueInfo> {
        deviceId?.let {
            return enterQueue(queueId, it, username)
        }
        return Result.Error(Exception("FID не установлен"))
    }

    private suspend fun enterQueue(
        queueId: String,
        deviceId: String,
        username: String
    ): Result<UserQueueInfo> {
        val result = safeApiCall { api.enterTheQueue(queueId, deviceId, username) }
        if (result is Result.Success) {
            dataStorage.setCurrentQueueId(result.data.queue.name)
        }
        return result
    }

    suspend fun leaveQueue(): Result<ResponseBody> {
        val queueId = getQueueId()
        queueId?.let {
            val result = safeApiCall {
                api.leaveTheQueue(
                    deviceId ?: throw IllegalArgumentException("FID не установлен")
                )
            }
            if (result is Result.Success) {
                dataStorage.clearCache()
            }
            return result
        }
        return Result.Error(Exception("Что-то пошло не так"))
    }

    suspend fun checkQueue(queueId: String) = safeApiCall { api.checkIfQueueIsActive(queueId) }
}