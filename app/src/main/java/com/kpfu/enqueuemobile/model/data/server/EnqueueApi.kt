package com.kpfu.enqueuemobile.model.data.server

import com.kpfu.enqueuemobile.model.entities.UserQueueInfo
import okhttp3.ResponseBody
import retrofit2.http.*

interface EnqueueApi {

    @POST("client/{queueId}")
    suspend fun enterTheQueue(
        @Path("queueId") queueId: String,
        @Query("deviceId") deviceId: String,
        @Query("username") username: String,
    ): UserQueueInfo

    @GET("client")
    suspend fun getUserQueueInfo(
        @Query("deviceId") deviceId: String,
    ): UserQueueInfo

    @DELETE("client")
    suspend fun leaveTheQueue(
        @Query("deviceId") deviceId: String,
    ): ResponseBody

    @GET("manage/isExist/{queueId}")
    suspend fun checkIfQueueIsActive(
        @Path("queueId") queueId: String
    ): Boolean
}
