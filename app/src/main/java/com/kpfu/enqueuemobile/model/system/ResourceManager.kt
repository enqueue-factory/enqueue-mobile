package com.kpfu.enqueuemobile.model.system

interface ResourceManager {
    fun getString(resId: Int): String
}