package com.kpfu.enqueuemobile.model.data.storage

import kotlinx.coroutines.flow.Flow

interface DataStorage {

    fun getQueueIdFlow(): Flow<String?>

    fun getCurrentQueueId(): String?

    fun setCurrentQueueId(id: String)

    fun clearCache()
}
