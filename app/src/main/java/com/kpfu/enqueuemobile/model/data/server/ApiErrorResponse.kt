package com.kpfu.enqueuemobile.model.data.server

import com.google.gson.annotations.SerializedName

data class ApiErrorResponse(
    @SerializedName("message")
    val message: String
)
