package com.kpfu.enqueuemobile.model.entities

import com.google.gson.annotations.SerializedName

data class UserQueueInfo(
    @SerializedName("place")
    val place: Int,
    @SerializedName("queue")
    val queue: Queue,
    @SerializedName("user")
    val user: User
)
