package com.kpfu.enqueuemobile.model.entities

import com.google.gson.annotations.SerializedName

data class User(
    @SerializedName("deviceId")
    val deviceId: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("identifier")
    val ticket: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("queue")
    val queue: Queue
)
