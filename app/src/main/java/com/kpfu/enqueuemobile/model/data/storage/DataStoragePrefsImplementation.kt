package com.kpfu.enqueuemobile.model.data.storage

import android.content.Context
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import com.kpfu.enqueuemobile.extensions.dataStore
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch

class DataStoragePrefsImplementation(private val context: Context) : DataStorage {

    private val QUEUE_ID_KEY = stringPreferencesKey("queue_id")

    var queueId: String? = null
    val flow = context.dataStore.data.map { prefs -> prefs[QUEUE_ID_KEY] }

    init {

        CoroutineScope(Dispatchers.Default).launch {
            flow.collect { string ->
                queueId = string
            }
        }
    }

    override fun getQueueIdFlow(): Flow<String?> = flow

    override fun getCurrentQueueId(): String? = queueId

    override fun setCurrentQueueId(id: String) {
        CoroutineScope(Dispatchers.IO).launch {
            context.dataStore.edit { prefs ->
                prefs[QUEUE_ID_KEY] = id
            }
        }
    }

    override fun clearCache() {
        CoroutineScope(Dispatchers.IO).launch {
            context.dataStore.edit { prefs ->
                prefs.clear()
            }
        }
    }
}