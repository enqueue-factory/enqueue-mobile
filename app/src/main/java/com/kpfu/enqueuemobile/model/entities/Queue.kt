package com.kpfu.enqueuemobile.model.entities

import com.google.gson.annotations.SerializedName

data class Queue(
    @SerializedName("active")
    val active: Boolean,
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String
)
