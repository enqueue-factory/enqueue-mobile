package com.kpfu.enqueuemobile.model.entities

data class ServerInfo(
    var protocol: String,
    var host: String,
    var port: Int,
)
