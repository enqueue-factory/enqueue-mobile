package com.kpfu.enqueuemobile.model.system

import android.content.Context

class ResourceManagerImplementation(private val context: Context) : ResourceManager {

    override fun getString(resId: Int): String = context.getString(resId)

}
