package com.kpfu.enqueuemobile.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import com.kpfu.enqueuemobile.R
import com.kpfu.enqueuemobile.presentation.home.HomeScreenViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeFragment : Fragment() {

    val viewModel: HomeScreenViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.activity_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initClickListeners(view)

    }

    private fun initClickListeners(view: View) {
        val qrButton = view.findViewById<Button>(R.id.btnQrScanner)
        qrButton.setOnClickListener { viewModel.onQrButtonClicked() }
    }
}
