package com.kpfu.enqueuemobile.ui.active

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import com.kpfu.enqueuemobile.R
import com.kpfu.enqueuemobile.presentation.active.ActiveScreenViewModel
import com.kpfu.enqueuemobile.presentation.states.ActiveScreenState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class ActiveFragment : Fragment() {

    val viewModel: ActiveScreenViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.activity_active, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initClickListeners(view)
        initStateListener()
        initDataListener(view)
    }

    private fun initDataListener(view: View) {
        lifecycleScope.launchWhenStarted {
            viewModel.queueInfoFlow.collect { userInfo ->
                launch(Dispatchers.Main) {
                    userInfo?.let {
                        view.findViewById<TextView>(R.id.tvUsernameLabel).text = it.user.name
                        view.findViewById<TextView>(R.id.tvTicketId).text = it.user.ticket
                        view.findViewById<TextView>(R.id.tvNumberOfPeopleBefore).text =
                            it.place.toString()
                    }
                }
            }
        }
    }

    private fun initStateListener() {
        var snack: Snackbar? = null
        lifecycleScope.launchWhenStarted {
            viewModel.activeScreenState.collect { state ->
                when (state) {
                    is ActiveScreenState.Error -> {
                        snack = Snackbar.make(
                            requireView(),
                            state.message,
                            Snackbar.LENGTH_INDEFINITE
                        )
                        snack?.show()
                    }
                    ActiveScreenState.Normal -> snack?.dismiss()
                    ActiveScreenState.ExitConfirmation -> {
                        MaterialAlertDialogBuilder(requireContext())
                            .setTitle(getString(R.string.active_screen_leave_queue_dialog_title))
                            .setPositiveButton(getString(R.string.active_screen_leave_queue_dialog_positive_button)) { di, _ ->
                                viewModel.onLeaveQueueConfirmed()
                                di.dismiss()
                            }
                            .setOnDismissListener { viewModel.onDialogDismissed() }
                            .show()
                    }
                }
            }
        }
    }

    private fun initClickListeners(view: View) {
        val leaveButton = view.findViewById<Button>(R.id.btnLeaveQueue)
        leaveButton.setOnClickListener { viewModel.onLeaveQueueButtonPressed() }
    }
}
