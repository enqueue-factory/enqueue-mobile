package com.kpfu.enqueuemobile.ui.splash

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.google.android.material.snackbar.Snackbar
import com.kpfu.enqueuemobile.R
import com.kpfu.enqueuemobile.presentation.splash.SplashScreenViewModel
import com.kpfu.enqueuemobile.presentation.states.BaseState
import kotlinx.coroutines.flow.collect
import org.koin.androidx.viewmodel.ext.android.viewModel

class SplashFragment : Fragment() {

    private val viewModel: SplashScreenViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.activity_splash, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initStateObservers(view)
    }

    private fun initStateObservers(view: View) {
        var snack: Snackbar? = null
        lifecycleScope.launchWhenCreated {
            viewModel.screenState.collect {
                if (it is BaseState.Warning) {
                    snack = Snackbar.make(view, it.message, Snackbar.LENGTH_INDEFINITE)
                    snack?.show()
                } else {
                    snack?.dismiss()
                }
            }
        }
    }
}
