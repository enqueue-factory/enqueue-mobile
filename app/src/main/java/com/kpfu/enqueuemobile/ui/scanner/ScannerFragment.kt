package com.kpfu.enqueuemobile.ui.scanner

import android.Manifest
import android.annotation.SuppressLint
import android.content.DialogInterface
import android.content.res.Configuration
import android.os.Bundle
import android.util.Size
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageProxy
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.view.PreviewView
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.textfield.TextInputEditText
import com.google.mlkit.vision.barcode.BarcodeScanning
import com.google.mlkit.vision.common.InputImage
import com.kpfu.enqueuemobile.R
import com.kpfu.enqueuemobile.extensions.showToast
import com.kpfu.enqueuemobile.extensions.visible
import com.kpfu.enqueuemobile.presentation.scanner.ScannerScreenViewModel
import com.kpfu.enqueuemobile.presentation.states.BaseState
import com.kpfu.enqueuemobile.presentation.states.ScannerScreenState
import com.kpfu.enqueuemobile.ui.base.BaseFragment
import kotlinx.coroutines.flow.collect
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

typealias BarcodeListener = (barcode: String) -> Unit

class ScannerFragment : BaseFragment() {

    private val viewModel: ScannerScreenViewModel by viewModel()

    private lateinit var cameraExecutor: ExecutorService
    private lateinit var previewView: PreviewView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.activity_scanner, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initClickListeners(view)
        initCamera()
        initStateObservers(view)
    }

    private fun initCamera() {
        previewView = requireView().findViewById(R.id.pvPreview)

        withPermission(Manifest.permission.CAMERA, {
            startCamera()
        }, {
            requireContext().showToast(getString(R.string.scanner_screen_activity_permission_needed_message))
            viewModel.onBackPressed()
        })
        cameraExecutor = Executors.newSingleThreadExecutor()
    }

    private fun initClickListeners(view: View) {
        val backButton = view.findViewById<Button>(R.id.btnBack)
        backButton.setOnClickListener { viewModel.onBackPressed() }
    }

    private fun initStateObservers(view: View) {
        lifecycleScope.launchWhenResumed {
            viewModel.scannerScreenState.collect { state ->
                when (state) {
                    ScannerScreenState.QueueDialog -> showQueueDialog()
                    is BaseState.Warning -> showWarning(state.message)
                    else -> {
                    }
                }
            }
        }
        lifecycleScope.launchWhenStarted {
            viewModel.scannerScreenState.collect {
                val boolean = it is BaseState.Loading
                view.findViewById<View>(R.id.mcvProgressWindow).visible(boolean)
            }
        }
    }

    private fun showWarning(message: String) {
        MaterialAlertDialogBuilder(requireContext())
            .setMessage(message)
            .setPositiveButton(getString(R.string.scanner_screen_warning_positive_button)) { di, _ -> di.dismiss() }
            .setOnDismissListener { viewModel.onDialogDismissed() }
            .show()
    }

    private fun showQueueDialog() {
        val inflatedView = LayoutInflater.from(requireContext())
            .inflate(
                R.layout.edit_text_dialog,
                requireView() as ViewGroup,
                false
            )

        MaterialAlertDialogBuilder(requireContext())
            .setTitle(getString(R.string.scanner_screen_enter_queue_dialog_title))
            .setView(inflatedView)
            .setPositiveButton(getString(R.string.scanner_screen_enter_queue_dialog_positive_button)) { _: DialogInterface, _: Int ->
                viewModel.onNameEntered(inflatedView.findViewById<TextInputEditText>(R.id.tietName).text.toString())
            }
            .setNegativeButton(getString(R.string.scanner_screen_enter_queue_negative_button)) { dialogInterface: DialogInterface?, _: Int ->
                dialogInterface?.dismiss()
            }
            .setOnDismissListener { viewModel.onDialogDismissed() }
            .show()
    }

    private fun startCamera() {
        val cameraProviderFeature = ProcessCameraProvider.getInstance(requireContext())
        cameraProviderFeature.addListener(Runnable {
            val cameraProvider: ProcessCameraProvider = cameraProviderFeature.get()

            val size =
                if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
                    Size(1080, 1920)
                } else {
                    Size(1920, 1080)
                }

            val cameraSelector = CameraSelector.DEFAULT_BACK_CAMERA

            val preview = Preview.Builder()
                .setTargetResolution(size)
                .build()
                .also {
                    it.setSurfaceProvider(previewView.surfaceProvider)
                }

            val imageAnalysis = ImageAnalysis.Builder()
                .setTargetResolution(size)
                .setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST)
                .build()
                .also {
                    it.setAnalyzer(cameraExecutor, BarcodeAnalyzer { qrResult ->
                        previewView.post {
                            if (viewModel.scannerScreenState.value is ScannerScreenState.ActiveScanner) {
                                viewModel.onQrScanned(qrResult)
                            }
                        }
                    })
                }

            cameraProvider.unbindAll()
            cameraProvider.bindToLifecycle(
                this,
                cameraSelector,
                preview,
                imageAnalysis,
            )

        }, ContextCompat.getMainExecutor(requireContext()))
    }
}

class BarcodeAnalyzer(private val barcodeListener: BarcodeListener) : ImageAnalysis.Analyzer {
    // Get an instance of BarcodeScanner
    private val scanner = BarcodeScanning.getClient()

    @SuppressLint("UnsafeExperimentalUsageError", "UnsafeOptInUsageError")
    override fun analyze(imageProxy: ImageProxy) {
        val mediaImage = imageProxy.image
        if (mediaImage != null) {
            val image = InputImage.fromMediaImage(mediaImage, imageProxy.imageInfo.rotationDegrees)
            // Pass image to the scanner and have it do its thing
            scanner.process(image)
                .addOnSuccessListener { barcodes ->
                    // Task completed successfully
                    for (barcode in barcodes) {
                        barcodeListener(barcode.rawValue ?: "")
                    }
                }
                .addOnFailureListener {
                    // You should really do something about Exceptions
                }
                .addOnCompleteListener {
                    // It's important to close the imageProxy
                    imageProxy.close()
                }
        }
    }
}