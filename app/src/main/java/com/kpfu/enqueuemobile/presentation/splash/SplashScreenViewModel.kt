package com.kpfu.enqueuemobile.presentation.splash

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.github.terrakok.cicerone.Router
import com.google.firebase.installations.FirebaseInstallations
import com.kpfu.enqueuemobile.R
import com.kpfu.enqueuemobile.Screens
import com.kpfu.enqueuemobile.model.data.repository.MainRepository
import com.kpfu.enqueuemobile.model.data.server.Result
import com.kpfu.enqueuemobile.model.data.server.ServerError
import com.kpfu.enqueuemobile.model.system.ResourceManager
import com.kpfu.enqueuemobile.presentation.states.BaseState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class SplashScreenViewModel(
    private val repository: MainRepository,
    private val router: Router,
    private val rm: ResourceManager
) : ViewModel() {

    val screenState = MutableStateFlow<BaseState>(BaseState.Loading)

    init {
        viewModelScope.launch {
            FirebaseInstallations.getInstance().id.addOnCompleteListener {
                if (it.isSuccessful) {
                    repository.deviceId = it.result
                    viewModelScope.launch {
                        repository.getUserQueueInfo().collect { result ->
                            if (result is Result.Success) {
                                router.newRootScreen(Screens.ActiveScreen())
                            } else if (result is Result.Error) {
                                if (result.exception is ServerError) {
                                    router.newRootScreen(Screens.HomeScreen())
                                } else {
                                    screenState.value =
                                        BaseState.Warning(rm.getString(R.string.splash_screen_connection_error_warning))
                                }
                            }
                        }
                    }
                } else {
                    screenState.value =
                        BaseState.Warning(rm.getString(R.string.splash_screen_fid_error_warning))
                }
            }
        }
    }
}
