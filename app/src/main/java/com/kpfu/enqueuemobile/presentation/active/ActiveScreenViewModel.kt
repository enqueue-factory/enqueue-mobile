package com.kpfu.enqueuemobile.presentation.active

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.github.terrakok.cicerone.Router
import com.kpfu.enqueuemobile.R
import com.kpfu.enqueuemobile.Screens
import com.kpfu.enqueuemobile.model.data.repository.MainRepository
import com.kpfu.enqueuemobile.model.data.server.Result
import com.kpfu.enqueuemobile.model.data.server.ServerError
import com.kpfu.enqueuemobile.model.entities.UserQueueInfo
import com.kpfu.enqueuemobile.model.system.ResourceManager
import com.kpfu.enqueuemobile.presentation.states.ActiveScreenState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class ActiveScreenViewModel(
    private val repository: MainRepository,
    private val router: Router,
    private val rm: ResourceManager
) :
    ViewModel() {
    private val _queueInfoFlow = MutableStateFlow<UserQueueInfo?>(null)
    val queueInfoFlow: StateFlow<UserQueueInfo?> = _queueInfoFlow

    private val _activeScreenState = MutableStateFlow<ActiveScreenState>(ActiveScreenState.Normal)
    val activeScreenState: StateFlow<ActiveScreenState> = _activeScreenState

    init {
        viewModelScope.launch {
            repository.queueInfoFlow.collect {
                when (it) {
                    is Result.Error -> {
                        if (it.exception is ServerError) {
                            router.newRootScreen(Screens.HomeScreen())
                        } else {
                            _activeScreenState.value =
                                ActiveScreenState.Error(rm.getString(R.string.active_screen_connection_error_warning))
                        }
                    }
                    is Result.Success -> {
                        _activeScreenState.value = ActiveScreenState.Normal
                        _queueInfoFlow.value = it.data
                    }
                }
            }
        }
    }

    fun onLeaveQueueButtonPressed() {
        _activeScreenState.value = ActiveScreenState.ExitConfirmation
    }

    fun onLeaveQueueConfirmed() {
        viewModelScope.launch {
            val res = repository.leaveQueue()

            if (res is Result.Success) {
                router.newRootScreen(Screens.HomeScreen())
            }
        }
    }

    fun onDialogDismissed() {
        _activeScreenState.value = ActiveScreenState.Normal
    }
}