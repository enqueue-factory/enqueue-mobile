package com.kpfu.enqueuemobile.presentation.states

sealed class BaseState : ScannerScreenState {
    object Loading : BaseState()
    data class Warning(val message: String) : BaseState()
}
