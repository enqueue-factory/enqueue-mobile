package com.kpfu.enqueuemobile.presentation.states

sealed interface ActiveScreenState {
    object Normal : ActiveScreenState
    data class Error(val message: String) : ActiveScreenState
    object ExitConfirmation : ActiveScreenState
}
