package com.kpfu.enqueuemobile.presentation.scanner

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.github.terrakok.cicerone.Router
import com.kpfu.enqueuemobile.R
import com.kpfu.enqueuemobile.Screens
import com.kpfu.enqueuemobile.model.data.repository.MainRepository
import com.kpfu.enqueuemobile.model.data.server.Result
import com.kpfu.enqueuemobile.model.system.ResourceManager
import com.kpfu.enqueuemobile.presentation.states.BaseState
import com.kpfu.enqueuemobile.presentation.states.ScannerScreenState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class ScannerScreenViewModel(
    private val repository: MainRepository,
    private val router: Router,
    private val rm: ResourceManager
) : ViewModel() {
    private val _scannerScreenState =
        MutableStateFlow<ScannerScreenState>(ScannerScreenState.ActiveScanner)
    val scannerScreenState: StateFlow<ScannerScreenState> = _scannerScreenState

    private var queueIdMem: String? = null

    fun onDialogDismissed() {
        queueIdMem = null
        _scannerScreenState.value = ScannerScreenState.ActiveScanner
    }

    fun onNameEntered(name: String) {
        viewModelScope.launch {
            _scannerScreenState.value = BaseState.Loading
            if (queueIdMem == null) {
                _scannerScreenState.value =
                    BaseState.Warning(rm.getString(R.string.scanner_screen_queueid_is_not_set_error_warning))
                return@launch
            }
            val queueId = queueIdMem ?: return@launch
            _scannerScreenState.value = BaseState.Loading
            val res = repository.enterQueue(queueId, name)

            if (res is Result.Success) {
                router.newRootScreen(Screens.ActiveScreen())
            } else {
                _scannerScreenState.value =
                    BaseState.Warning(rm.getString(R.string.scanner_screen_enter_queue_response_error_warning))
            }
        }
    }

    fun onQrScanned(code: String) {
        viewModelScope.launch {
            _scannerScreenState.value = BaseState.Loading
            val res = repository.checkQueue(code)
            if (res is Result.Success) {
                if (res.data) {
                    queueIdMem = code
                    _scannerScreenState.value = ScannerScreenState.QueueDialog
                } else {
                    _scannerScreenState.value =
                        BaseState.Warning(rm.getString(R.string.scanner_screen_no_such_queue_error_warning))
                }
            } else {
                _scannerScreenState.value =
                    BaseState.Warning(rm.getString(R.string.scanner_screen_connection_error_warning))
            }
        }
    }

    fun onBackPressed() {
        router.exit()
    }

}