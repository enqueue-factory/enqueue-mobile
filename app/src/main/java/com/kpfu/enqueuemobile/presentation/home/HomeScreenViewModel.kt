package com.kpfu.enqueuemobile.presentation.home

import androidx.lifecycle.ViewModel
import com.github.terrakok.cicerone.Router
import com.kpfu.enqueuemobile.Screens

class HomeScreenViewModel(private val router: Router) : ViewModel() {

    fun onQrButtonClicked() {
        router.navigateTo(Screens.ScannerScreen())
    }
}