package com.kpfu.enqueuemobile.presentation.states

sealed interface ScannerScreenState {
    object ActiveScanner : ScannerScreenState
    object QueueDialog : ScannerScreenState
}